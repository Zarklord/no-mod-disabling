--[[
Copyright (C) 2020 Zarklord

This file is part of Action Hold.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details
The source codes does not come with any warranty including
the implied warranty of merchandise.
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]
name = "No Mod Disabling"
version = "1.0"
description = [[
When this mod is force enabled, mods will not get disabled when the game crashes during loading.
This also removes the mod warning when loading the game.
]]
author = "Zarklord"

restart_required = false

dst_compatible = true

api_version_dst = 10

folder_name = folder_name or "workshop-"
if not folder_name:find("workshop-") then
    name = "1 "..name.." - GitLab Version"
    version = version.."G"
end

priority = 3.4028e+38

icon_atlas = "nomoddisabling.xml"
icon = "nomoddisabling.tex"

all_clients_require_mod = false
client_only_mod = true

configuration_options = {}